package com.sokhan.sokhanassignment.interfaces


/**
 * Created by Amin on 27/11/2017.
 */
interface NetworkListeners {

    abstract fun onResponse(response: String)
    abstract fun onError(message: String, isOnline: Boolean)
}