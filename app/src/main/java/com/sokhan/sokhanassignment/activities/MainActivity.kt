package com.sokhan.sokhanassignment.activities

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.content.Intent
import android.widget.Toast
import android.content.ActivityNotFoundException
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.sokhan.sokhanassignment.Network.NetworkMocker
import com.sokhan.sokhanassignment.R
import com.sokhan.sokhanassignment.interfaces.NetworkListeners
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


class MainActivity : AppCompatActivity() {


    private lateinit var mDrawerToggle: ActionBarDrawerToggle
    private val REQ_CODE_SPEECH_INPUT = 100
    var prevCommand = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById<Toolbar>(R.id.toolbar))
        setDrawer()
        microphone.setOnClickListener({promptSpeechInput()})
        keyboard.setOnClickListener({showKeyboardDialog()})
        responseEntered(getString(R.string.how_can_I_help))
    }

    private fun setDrawer() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        mDrawerToggle = object : ActionBarDrawerToggle(this, root,
                R.string.drawer_open, R.string.drawer_close) {
        }
        mDrawerToggle.isDrawerIndicatorEnabled = true
        root.addDrawerListener(mDrawerToggle)
        mDrawerToggle.syncState()
    }


    private fun showKeyboardDialog() {
        val convertView = LayoutInflater.from(this)
                .inflate(R.layout.dialog_new_command, root, false)
        val input = convertView.findViewById<View>(R.id.input) as EditText
        MaterialDialog.Builder(this).customView(convertView, true)
                .onPositive { _, _ -> commandEntered(input.text.toString()) }.positiveText(getString(R.string.create))
                .buttonRippleColor(resources.getColor(R.color.colorPrimaryDark))
                .negativeColor(resources.getColor(R.color.colorPrimaryDark))
                .positiveColor(resources.getColor(R.color.colorPrimaryDark))
                .negativeText(getString(R.string.cancel))
                .typeface("theme.ttf", "theme_light.ttf").build().show()
    }

    private fun responseEntered(message: String) {
        createChatBubble(message, true)
    }

    private fun commandEntered(command: String) {
        createChatBubble(command, false)
        processCommand(command)
    }

    private fun processCommand(command: String) {
        val paramsJson = JSONObject()
        paramsJson.put("command", command)
        paramsJson.put("prevCommand", prevCommand)
        prevCommand = command
        val row = LayoutInflater.from(this).inflate(R.layout.item_chat_loading_right, messagesLay,
                false)
        messagesLay.addView(row)
        NetworkMocker.getRequest("www.amin.com/process", object : NetworkListeners{
            override fun onResponse(response: String) {
                messagesLay.removeView(row)
                responseEntered(response)
            }

            override fun onError(message: String, isOnline: Boolean) {
                messagesLay.removeView(row)
                responseEntered(message.substring(1))
            }

        }, paramsJson)
    }

    private fun createChatBubble(command: String, rightOrLeft: Boolean) {
        var layout = R.layout.item_chat_bubble_right
        if (!rightOrLeft){
            layout = R.layout.item_chat_bubble_left
        }
        val row = LayoutInflater.from(this).inflate(layout, messagesLay,
                false)
        row.findViewById<TextView>(R.id.message).text = command
        if (messagesLay.childCount > 3) {
            messagesLay.removeViewAt(0)
        }
        messagesLay.addView(row)
    }

    /**
     * Showing google speech input dialog
     */
    private fun promptSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "fa-IR")
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt))
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(applicationContext,
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show()
        }

    }

    /**
     * Receiving speech input
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQ_CODE_SPEECH_INPUT -> {
                if (resultCode == Activity.RESULT_OK && null != data) {

                    val result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    commandEntered(result[0])
                }
            }
        }
    }
}
