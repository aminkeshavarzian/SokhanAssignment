package com.sokhan.sokhanassignment.Network

import android.os.Handler
import com.sokhan.sokhanassignment.interfaces.NetworkListeners
import org.json.JSONObject
import java.util.*

/**
 * Created by Amin on 26/04/2018.
 *
 */
class NetworkMocker {


    companion object {

        private const val nextCommandTemplate = "، دیگه چی ؟"

        public fun getRequest(url: String, listener: NetworkListeners, paramsJson: JSONObject) {

            Handler().postDelayed({
                val response = getMockAnswer(paramsJson.getString("command"),
                        paramsJson.getString("prevCommand"))
                if (response.startsWith("e")) {
                    listener.onError(response, true)
                } else {
                    listener.onResponse(response)
                }
            }, 1500)

        }

        private fun getMockAnswer(message: String, prevMessage: String): String {
            return when (message) {
                "کولر را خاموش کن" -> "چشم$nextCommandTemplate"
                "خاموش کن" -> "دستور خاموش کردن برای کدام دستگاه میباشد؟"
                "روشن کن" -> "دستور روشن کردن برای کدام دستگاه میباشد؟"
                "چراغ های حمام" -> {
                    equipmentSwitch(message, prevMessage)
                }
                "کولر" -> {
                    equipmentSwitch(message, prevMessage)
                }
                "بخاری" -> {
                    equipmentSwitch(message, prevMessage)
                }
                "تلویزیون" -> {
                    equipmentSwitch(message, prevMessage)
                }
                "چراغ" -> {
                    equipmentSwitch(message, prevMessage)
                }
                "شوفاژ" -> {
                    equipmentSwitch(message, prevMessage)
                }
                else -> "eمتاسفانه در این زمینه کمکی از من بر نمیاد، دوباره تلاش کنید!"
            }
        }

        private fun equipmentSwitch(message: String, prevMessage: String): String {
            return when (prevMessage) {
                "خاموش کن" -> "دستور خاموش کردن $message انجام شد$nextCommandTemplate"
                "روشن کن" -> "دستور روشن کردن $message انجام شد$nextCommandTemplate"
                else -> "e$prevMessage این دستور را پشتیبانی نمیکند، دوباره تلاش کنید!"
            }
        }
    }
}